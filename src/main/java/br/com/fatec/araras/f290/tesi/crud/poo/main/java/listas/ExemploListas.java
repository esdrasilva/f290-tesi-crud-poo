package br.com.fatec.araras.f290.tesi.crud.poo.main.java.listas;

import java.util.ArrayList;
import java.util.List;


public class ExemploListas {
    public static void main(String[] args) {
        
        List<Float> notas = new ArrayList<>();
        notas.add(9.0F);
        notas.add(8.6F);
        notas.add(10.F);
        notas.add(7.89F);
        notas.add(9.0F);
        notas.add(8.6F);
        notas.add(10.F);
        notas.add(7.89F);
                
        System.out.println("Alguem tirou 10? "+ (notas.contains(10F)));
        
        System.out.println(notas.get(2));
       
        
        notas.forEach(nota -> {
            System.out.println(nota);
        });
        
    }
}
