package br.com.fatec.araras.f290.tesi.crud.poo.main.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConectionFactory {
    
    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/fatec";
        String user = "root";
        String password = "donotcross";
        
        return DriverManager.getConnection(url, user, password);
    }
}
