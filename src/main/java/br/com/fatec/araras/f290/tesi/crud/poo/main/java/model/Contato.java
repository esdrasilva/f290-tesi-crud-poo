
package br.com.fatec.araras.f290.tesi.crud.poo.main.java.model;


public class Contato {
    
    private Long id;
    private String nome;
    private String email;
    private String telefone;
    private String facebook;
    private String linkedin;

    public Contato(String nome, String email, String telefone) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    public Contato(Long id, String nome, String email, String telefone, String facebook, String linkedin) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.facebook = facebook;
        this.linkedin = linkedin;
    }

    public Contato() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    @Override
    public String toString() {
        return "Contato{" + "id=" + id + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone + ", facebook=" + facebook + ", linkedin=" + linkedin + '}';
    }
    
}
