package br.com.fatec.araras.f290.tesi.crud.poo.main.java;

import br.com.fatec.araras.f290.tesi.crud.poo.main.java.model.Contato;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ContatoDAO {
    public void salvar(Contato contato){
        
        final String sql = "insert into contato (nome, email, telefone) values ('%s','%s','%s')";
        
        try (Connection conexao = ConectionFactory.getConnection();
                Statement statement = conexao.createStatement()) {
                
                String insertContato = String.format(sql, contato.getNome(), contato.getEmail(), contato.getTelefone());

                statement.execute(insertContato);
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, "Erro ao salvar contato.", ex);
        }
    }
    
    public void excluir(Long idContato){
        
        final String sql = "delete from contato where id = %d";
        
        try (Connection conexao = ConectionFactory.getConnection();
                Statement statement = conexao.createStatement()) {
                
                String insertContato = String.format(sql, idContato);

                statement.execute(insertContato);
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, "Erro ao excluir contato.", ex);
        }
    }
    
    public Contato buscarPorId(Long id) {
        final String sql  = "select id, nome, email, telefone, facebook, linkedin from contato where id = %d";
        Contato contato = new Contato();
        
        try(Connection conexao = ConectionFactory.getConnection();
                Statement statement = conexao.createStatement()) {
            
            ResultSet resultSet = statement.executeQuery(String.format(sql, id));            
            
            if(resultSet.next()) {
                contato.setId(resultSet.getLong("id"));
                contato.setNome(resultSet.getString("nome"));
                contato.setEmail(resultSet.getString("email"));
                contato.setFacebook(resultSet.getString("facebook"));
                contato.setLinkedin(resultSet.getString("linkedin"));
            }          
                        
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, "Falha ao consultar a entidade contato.", ex);
        } 
        return contato;
    }
    
    public List<Contato> buscarTodos() {
        final String sql  = "select distinct id, nome, email, telefone, facebook, linkedin from contato";
        
        List<Contato> contatos = new ArrayList<>();
        
        try(Connection conexao = ConectionFactory.getConnection();
                Statement statement = conexao.createStatement();
                ResultSet resultSet = statement.executeQuery(sql)) {
            
            while(resultSet.next()) {
                Contato contato = new Contato();
                
                contato.setId(resultSet.getLong("id"));
                contato.setNome(resultSet.getString("nome"));
                contato.setEmail(resultSet.getString("email"));
                contato.setEmail(resultSet.getString("telefone"));
                contato.setFacebook(resultSet.getString("facebook"));
                contato.setLinkedin(resultSet.getString("linkedin"));
                
                contatos.add(contato);
            }          
                        
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, "Falha ao consultar a entidade contato.", ex);
        }
        return contatos;
    }
    
    public void atualizar(Contato contato) {
        final String sql = "update contato set nome = %s, email = %s, telefone = %s, facebook = %s, linkedin = %s where id = %d";
        
        try(Connection conexao = ConectionFactory.getConnection();
                Statement statement = conexao.createStatement()) {
            
            statement.execute(String.format(sql, 
                    contato.getNome(), 
                    contato.getEmail(),
                    contato.getTelefone(), 
                    contato.getFacebook(), 
                    contato.getLinkedin(),
                    contato.getId()));
           
            } catch (SQLException ex) {          
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }          
    }
}
