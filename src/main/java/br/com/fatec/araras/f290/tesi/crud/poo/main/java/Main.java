package br.com.fatec.araras.f290.tesi.crud.poo.main.java;

import br.com.fatec.araras.f290.tesi.crud.poo.main.java.model.Contato;
import java.util.List;


public class Main {
    public static void main(String[] args)  {
        ContatoDAO contatoDAO = new ContatoDAO();        
//        Contato contato = new Contato("Alvin Satnana", "alvin@gamil.com", "99999-7777");        
//        contatoDAO.salvar(contato);
//        contatoDAO.excluir(3L);
//        Contato contato = contatoDAO.buscarPorId(2L);
        List<Contato> todos = contatoDAO.buscarTodos();
        System.out.println(todos);
    }
}
